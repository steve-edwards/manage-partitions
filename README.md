# manage-partitions

A 'missing' utility' that should be part of MySQL that re-organizes
date range partitions.

This command line utility manages MySQL partitions. It can create new
daily partitions, roll up daily partitions to monthly partitions, and
roll up monthly partitions to yearly partitions -- all stuff MySQL
should just do, but doesn't.

Manage partitions

	--help				Basic usage.
	--full-help			More detailed help.
	--future-help			Unimplemented features...

	--database=<database-name>	Set the database.
	--host=<host-name>		Set the host.
	--login-path=<login-path>	Set the login path.
	--password=<password>		Set the password.
	--table=<table-name>		Set the table.
	--user=<user-name>		Set the user.

	--days=<n>			How many days before rollup.
	--months=<n>			How many months before rollup.

	--debug				Make debugging easier.
	--disable-binlog		Disable bin log for each session.
	--examples			Show an example command.
	--stop-on-error			Add 'set -e' to the script
					output.
					(Highly recommended.)
	--verbose			Include statements to log
					progress in the script output.
					(Highly recommended.)

# (end of README.md)
