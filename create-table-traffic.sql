--
-- Table structure for table `Traffic`
--

DROP TABLE IF EXISTS `Traffic`;
CREATE TABLE `Traffic` (
--  `hit_id` int(11) NOT NULL AUTO_INCREMENT,
  `hit_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `producer_id` int(11) NOT NULL DEFAULT '0',
--  `store_type` char(12) NOT NULL DEFAULT '',
  `store_type` enum ('', 'clip', 'image', 'video') not null default '',
  `hit_time` datetime NOT NULL DEFAULT '0000-01-01 00:00:00',
  `remote_host` varchar(30) NOT NULL DEFAULT '',
  `host_from` varchar(300) DEFAULT NULL,
  `parent_domain` varchar(55) NOT NULL,
  `tld` varchar(85) NOT NULL,
  `query` varchar(35) NOT NULL DEFAULT '',
--  PRIMARY KEY (`hit_id`),
  PRIMARY KEY (`hit_id`, `hit_time`),
  KEY `hit_time` (`hit_time`),
  KEY `stats` (`store_type`,`producer_id`,`hit_time`)
)
	key_block_size			= 8
	row_format			= compressed
--	partition by range columns (hit_time)
	partition by range (to_days(hit_time))
		(
		  partition p2016 values less than (to_days('2017-01-01'))
		, partition p2017 values less than (to_days('2018-01-01'))
		, partition p2018 values less than (to_days('2019-01-01'))
		, partition p2019_01 values less than (to_days('2019-02-01'))
		, partition p2019_02_01 values less than (to_days('2019-02-02'))
		, partition p2019_02_02 values less than (to_days('2019-02-03'))
		, partition p2019_02_03 values less than (to_days('2019-02-04'))
		, partition p2019_02_04 values less than (to_days('2019-02-05'))
		, partition p2019_02_05 values less than (to_days('2019-02-06'))
		, partition p2019_02_06 values less than (to_days('2019-02-07'))
		, partition p2019_02_07 values less than (to_days('2019-02-08'))
		, partition p2019_02_08 values less than (to_days('2019-02-09'))
		, partition p2019_02_09 values less than (to_days('2019-02-10'))
		, partition p2019_02_10 values less than (to_days('2019-02-11'))
		, partition p2019_02_11 values less than (to_days('2019-02-12'))
		, partition p2019_02_12 values less than (to_days('2019-02-13'))
		, partition p2019_02_13 values less than (to_days('2019-02-14'))
		, partition p2019_02_14 values less than (to_days('2019-02-15'))
		, partition p2019_02_15 values less than (to_days('2019-02-16'))
		, partition p2019_02_16 values less than (to_days('2019-02-17'))
		, partition p2019_02_17 values less than (to_days('2019-02-18'))
		, partition p2019_02_18 values less than (to_days('2019-02-19'))
		, partition p2019_02_19 values less than (to_days('2019-02-20'))
		, partition p2019_02_20 values less than (to_days('2019-02-21'))
		, partition p2019_02_21 values less than (to_days('2019-02-22'))
		, partition p2019_02_22 values less than (to_days('2019-02-23'))
		, partition p2019_02_23 values less than (to_days('2019-02-24'))
		, partition p2019_02_24 values less than (to_days('2019-02-25'))
		, partition p2019_02_25 values less than (to_days('2019-02-26'))
		, partition p2019_02_26 values less than (to_days('2019-02-27'))
		, partition p2019_02_27 values less than (to_days('2019-02-28'))
		, partition p2019_02_28 values less than (to_days('2019-03-01'))
		, partition p2019_03_01 values less than (to_days('2019-03-02'))
		, partition p2019_03_02 values less than (to_days('2019-03-03'))
		, partition p2019_03_03 values less than (to_days('2019-03-04'))
		, partition p2019_03_04 values less than (to_days('2019-03-05'))
		, partition p2019_03_05 values less than (to_days('2019-03-06'))
		, partition p2019_03_06 values less than (to_days('2019-03-07'))
		, partition p2019_03_07 values less than (to_days('2019-03-08'))
		, partition p2019_03_08 values less than (to_days('2019-03-09'))
		, partition p2019_03_09 values less than (to_days('2019-03-10'))
		, partition p2019_03_10 values less than (to_days('2019-03-11'))
		, partition p2019_03_11 values less than (to_days('2019-03-12'))
		, partition p2019_03_12 values less than (to_days('2019-03-13'))
		, partition p2019_03_13 values less than (to_days('2019-03-14'))
		, partition p2019_03_14 values less than (to_days('2019-03-15'))
		, partition p2019_03_15 values less than (to_days('2019-03-16'))
		, partition p2019_03_16 values less than (to_days('2019-03-17'))
		, partition p2019_03_17 values less than (to_days('2019-03-18'))
		, partition p2019_03_18 values less than (to_days('2019-03-19'))
		, partition p2019_03_19 values less than (to_days('2019-03-20'))
		, partition p2019_03_20 values less than (to_days('2019-03-21'))
		, partition p2019_03_21 values less than (to_days('2019-03-22'))
		, partition p2019_03_22 values less than (to_days('2019-03-23'))
		, partition p2019_03_23 values less than (to_days('2019-03-24'))
		, partition p2019_03_24 values less than (to_days('2019-03-25'))
		, partition p2019_03_25 values less than (to_days('2019-03-26'))
		, partition p2019_03_26 values less than (to_days('2019-03-27'))
		, partition p2019_03_27 values less than (to_days('2019-03-28'))
		, partition p2019_03_28 values less than (to_days('2019-03-29'))
		, partition p2019_03_29 values less than (to_days('2019-03-30'))
		, partition p2019_03_30 values less than (to_days('2019-03-31'))
		, partition p2019_03_31 values less than (to_days('2019-04-01'))
		, partition p2019_04 values less than (to_days('2019-05-01'))
		, partition p2019_05 values less than (to_days('2019-06-01'))
		, partition p2019_06 values less than (to_days('2019-07-01'))
		, partition p2019_07 values less than (to_days('2019-08-01'))
		, partition p2019_08 values less than (to_days('2019-09-01'))
		, partition p2019_09 values less than (to_days('2019-10-01'))
		, partition p2019_10 values less than (to_days('2019-11-01'))
		, partition p2019_11 values less than (to_days('2019-12-01'))
		, partition p2019_12 values less than (to_days('2020-01-01'))
--		, partition future values less than maxvalue
		)
	;

-- (end of create-table-traffic.sql)
